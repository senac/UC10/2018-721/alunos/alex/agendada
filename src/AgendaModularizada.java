
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class AgendaModularizada {

    public static void showMessageInfo(String m) {

        JOptionPane.showMessageDialog(null, m, "Informação", JOptionPane.INFORMATION_MESSAGE);

    }

    public static void showMessageWarnning(String m) {
        JOptionPane.showMessageDialog(null, m, "Advertência", JOptionPane.WARNING_MESSAGE);
    }

    public static void showMessageErro(String m) {
        JOptionPane.showMessageDialog(null, m, "Erro", JOptionPane.ERROR_MESSAGE);
    }

    public static Contato pesquisaContato(List<Contato> lista, String nome) {
        if (!lista.isEmpty()) {

            for (Contato c : lista) {
                if (c.nome.equals(nome)) {
                    return c;
                }
            }

        }

        return null;
    }

    /*
    public static Contato pesquisaContato1(List<Contato> lista, String nome) {
        
        for (int i = 0; i < lista.size(); i++) {
            Contato c = lista.get(i);
            if (c.nome.equals(nome)) {
                return lista.get(i);
            }
        }

        return null;
    }*/

    public static void main(String[] args) {

        List<Contato> contatos = new ArrayList<>();
        String aux;
        int opcao;
        Contato contato = null;
        String nome;
        String telefone;
        for (;;) {
            boolean achei = false;

            String menu = String.format("########### Agenda de Contatos ###########\n"
                    + "                                Contatos(%d)\n"
                    + "1 - Novo Contato\n"
                    + "2 - Buscar Contato por Nome\n"
                    + "3 - Remover Contato\n"
                    + "4 - Lista Todos Contatos\n"
                    + "5 - Sobre Agenda de Contatos\n"
                    + "0 - Sair", contatos.size());

            aux = JOptionPane.showInputDialog(menu);
            opcao = Integer.parseInt(aux);

            switch (opcao) {
                case 1:
                    nome = JOptionPane.showInputDialog("Nome");

                   contato = pesquisaContato(contatos, nome) ; 

                    if (contato == null) {
                        telefone = JOptionPane.showInputDialog("Telefone");
                        contato = new Contato();
                        contato.nome = nome;
                        contato.telefone = telefone;

                        contatos.add(contato);

                        showMessageInfo("Salvo com sucesso!");

                    } else {
                        showMessageErro("Contato ja cadastrado!!!");
                    }

                    break;

                case 2:
                    if (!contatos.isEmpty()) {
                        nome = JOptionPane.showInputDialog("Digite Nome a ser buscado");
                        contato =pesquisaContato(contatos, nome) ; 

                        if (contato != null ) {
                            showMessageInfo(contato.nome + "\n" + contato.telefone);
                        } else {
                            showMessageInfo("Nome pesquisado nao encontrado!");
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "Lista Vazia!");
                    }

                    break;

                case 3:
                    if (!contatos.isEmpty()) {
                        
                        nome = JOptionPane.showInputDialog("Digite nome a ser removido");
                        contato = pesquisaContato(contatos, nome) ; 

                        if (contato != null ) {
                            contatos.remove(contato);
                            showMessageInfo("Removido com sucesso!");

                        } else {
                            showMessageErro("Nome pesquisado nao encontrado!");
                        }

                    } else {
                        showMessageErro("Lista Vazia!");
                    }

                    break;

                case 4:
                    //“NomeContato – (22)3222-2222” 
                    String listaDeContatos = new String();

                    for (int i = 0; i < contatos.size(); i++) {
                        listaDeContatos += contatos.get(i).nome + " – " + contatos.get(i).telefone + "\n";
                    }

                    JOptionPane.showMessageDialog(null, listaDeContatos);

                    break;

                case 5:

                    String sobre = "Desenvolvida por : Jose da Silva\n"
                            + "Turma: 721 de 2018 ";

                    JOptionPane.showMessageDialog(null, sobre);

                    break;

                case 0:
                    System.exit(0);

            }

        }

    }

}
